Readme file
I have created a  repository as required by the Assignment 01, 2019- Winter of the course Info-2300. I am using my HTML/CSS Majorassignment2 as a repository form the previous course for this assignment (Assignment 01-Info 2300). To open the repository file , Please do the following. 
1.	Please download the repository files from  https://bitbucket.org/shujaatmcse/info2300/downloads/     These are assignment files.
2.	Uzip the folder 
3.	Save the files to a folder that you could easily remember. 
4.	4. Double Click the default.html file in the folder which should open the files into the default browser.
5.	Requirements to use the repository: A browser google chrome or internet explorer is required to run these files.
To access Wiki Article, Readme File: click on source- left side link after you open the link https://bitbucket.org/shujaatmcse/info2300/downloads/ . 
Wiki article is also available on the home page when you click Wiki
Issues: Issues can be access by clicking Issue.

Product License: Reference https://opensource.org/licenses/GPL-3.0 
GNU GENERAL PUBLIC LICENSE: I selected this type of license because  The GNU General Public License is a free, copyleft license for software and other kinds of works.
The licenses for most software and other practical works are designed to take away your freedom to share and change the works. By contrast, the GNU General Public License is intended to guarantee your freedom to share and change all versions of a program--to make sure it remains free software for all its users. We, the Free Software Foundation, use the GNU General Public License for most of our software; it applies also to any other work released this way by its authors. You can apply it to your programs, too.
....